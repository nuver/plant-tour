const BASE_API_URL = 'http://planttour.cnh.desenv.uaify.com/api/';
// SOU CONCESSIONARIA -> STEP 1
$(document).on('click', '#btnSouConcessionaria', function () {
    $('#formDivAgendamentoSteps').show();
    steppers_active(1);
    $('#formDivAgendamentoIDDealer').show();
    $('#formDivAgendamentoInicio').hide();
});
//STEP 1 -> Confirmar GDD DEALER
$(document).on('click', '#btnAbrirModalAgendamento', function () {
    inputGDDealerNumber = $('#inputGDDealerNumber').val();
    axios.get(BASE_API_URL + 'concessionario/' + inputGDDealerNumber, {})
        .then(response => {
            resultado = response.data.Data.Results;
            if (resultado.DealerName === null) {
                localStorage.setItem("GDDealer", JSON.stringify({}));
                alert('Não encontrado.');
            } else {
                // localStorage.setItem("GDDealer", JSON.stringify(resultado));
                localStorage.setItem("GDDealer", JSON.stringify(inputGDDealerNumber));
                console.log("GD Dealer: ", resultado);
                $('#agendamentoModal').modal('toggle');
            }
        })
        .catch(error => {
            alert('Não encontrado.');
            // console.log(error);
        });
});
// GDD DEALER  -> STEP 2
$(document).on('click', '#btnAceiteRegras', function () {
    if (document.getElementById('cboxAceiteRegras').checked) {
        steppers_active(2);
        $('#formDivAgendamentoFabrica').show();
        $('#formDivAgendamentoIDDealer').hide();
        $('#agendamentoModal').modal('toggle');
    } else {
        alert("Para prosseguir no agendamento o cliente deve aceitar as regras descritas no regulamento.");
    }
});
// STEP 2 -> STEP 3
$(document).on('click', '#btnEscolheuFabrica', function () {
    steppers_active(3);
    codigoFabrica = $(".fabrica-selecionada").attr("codigo");
    localStorage.setItem("FABRICA", JSON.stringify({"codigo": codigoFabrica}));
    console.log("Código da fabrica:", codigoFabrica);
    $('#formDivAgendamentoDataDisponivel').show();
    $('#formDivAgendamentoFabrica').hide();
});
// STEP 3 -> STEP 4
$(document).on('click', '#btnEscolheuData', function () {
    steppers_active(4);
    dataVisita = $('#datepicker_input').val();
    localStorage.setItem("DATA_VISITA", JSON.stringify(dataVisita));
    console.log("Data visita:", dataVisita);
    $('#formDivAgendamentoDataDisponivel').hide();
    $('#formDivAgendamentoDadosAgendamento').show();
});

// STEP 4 -> Enviar
$(document).on('click', '#btnFinalizarVisita', function () {
    dealerID = JSON.parse(localStorage.getItem("GDDealer"));
    fabrica = JSON.parse(localStorage.getItem("FABRICA"));
    dataVisita = JSON.parse(localStorage.getItem("DATA_VISITA"));
    jsonData = {
        "data": {
            "Agendamento": {
                "DealerID": dealerID,
                "Concessionario": $("#inputNomeConcessionaria").val(),
                "Telefone": $("#inputTelefoneConcessionaria").val(),
                "Estado": $("#inputEstado").val(),
                "Cidade": $("#inputCidade").val(),
                "FabricaID": fabrica.codigo,
                "DataVisita": dataVisita,
                "Segmento": $("#selectSegmento").val()
            },
            "Responsavel": {
                "Email": $("#inputNomeResponsavel").val(),
                "Nome": $("#inputCargoresponsavel").val(),
                "Cargo": $("#inputEmailResponsavel").val(),
                "Telefone": $("#inputTelefoneFixoResponsavel").val(),
                "Celular": $("#inputTelefoneCelularResponsavel").val()
            }
        }
    };
    axios.post(BASE_API_URL + 'agendamentoVisita/', jsonData)
        .then(response => {
            console.log(response);
        })
        .catch(error => {
            console.log(error);
        });
});

// função do stepper
function steppers_active(active = 1, len = 4) {
    $('#divSteppers').html('');
    for (i = 1; i <= len; i++) {
        if (i == active) {
            $('#divSteppers').append(`<div class=\"badge badge-pill badge-secondary badge-active\">${i}</div> `);
        } else {
            $('#divSteppers').append(`<div class=\"badge badge-pill badge-secondary \">${i}</div> `);
        }
    }
}

// fabrica selecionada
$(document).on('click', '.cardFabrica', function () {
    $(".fabrica-selecionada").removeClass("fabrica-selecionada");
    $(this).addClass('fabrica-selecionada');
});