// Verifica se já existe um vetor de participantes
if (localStorage.getItem("participantes") === null) {
    localStorage.setItem("participantes", JSON.stringify([]));
    participantes = [];
} else {
    participantes = JSON.parse(localStorage.getItem("participantes"));
    if (participantes.length > 0) {
        listarParticipantes();
    }
}


// Salvar Participante
$(document).ready(function () { // Wait until document is fully parsed
    $("#formParticipante").on('submit', function (e) {
        e.preventDefault();
        participante = {
            nome: $("#inputNomeParticipante").val(),
            rg: $("#inputRGParticipante").val(),
            cpf: $("#inputCPFParticipante").val(),
            dataNasc: $("#dataNascimentoParticipante").val(),
            empresa: $("#inputEmpresaParticipante").val(),
            endereco: $("#inputEnderecoParticipante").val(),
            estado: $("#inputEstado").val(),
            cidade: $("#inputCidade").val(),
            email: $("#inputEmailParticipante").val(),
            celular: $("#inputTelefoneCelular").val(),
            tamanhoCamisa: $("#inputTamanhoCamisaParticipante").val(),
            tamanhoCalcado: $("#inputTamanhoCalcadoParticipante").val()
        };
        $(this).trigger("reset");
        if (localStorage.getItem("editando_participante") === null) {
            //novo registro
            participantes.push(participante);
            localStorage.setItem("participantes", JSON.stringify(participantes));
            $('#divParticipantesCadastrados').show();
            listarParticipantes();
            alert("Participante incluido com sucesso.");
        } else {
            //editando participante
            participantes[localStorage.getItem("editando_participante")] = participante;
            localStorage.setItem("participantes", JSON.stringify(participantes));
            listarParticipantes();
            localStorage.removeItem("editando_participante");
        }
    });
});

$(document).on('click', '.excluirParticipante', function () {
    var result = confirm("Realmente deseja remover o participante?");
    if (result) {
        participantes.splice($(this).attr('chave'), 1);
        localStorage.setItem("participantes", JSON.stringify(participantes));
        $('#divParticipantesCadastrados').show();
        listarParticipantes();
    }
});

$(document).on('click', '.editarParticipante', function () {
    chave = $(this).attr('chave');
    $("#inputNomeParticipante").val(participantes[chave].nome);
    $("#inputRGParticipante").val(participantes[chave].rg);
    $("#inputCPFParticipante").val(participantes[chave].cpf);
    $("#dataNascimentoParticipante").val(participantes[chave].dataNasc);
    $("#inputEmpresaParticipante").val(participantes[chave].empresa);
    $("#inputEnderecoParticipante").val(participantes[chave].endereco);
    $("#inputEstado").val(participantes[chave].estado);
    $("#inputCidade").val(participantes[chave].cidade);
    $("#inputEmailParticipante").val(participantes[chave].email);
    $("#inputTelefoneCelular").val(participantes[chave].celular);
    $("#inputTamanhoCamisaParticipante").val(participantes[chave].tamanhoCamisa);
    $("#inputTamanhoCalcadoParticipante").val(participantes[chave].tamanhoCalcado);
    localStorage.setItem("editando_participante", chave);
});


function listarParticipantes() {
    if (participantes.length > 0) {
        $('#divParticipantesCadastrados').show();
        $('#divParticipantesLista').html('');
        participantes.forEach(function (valor, chave) {
            $('#divParticipantesLista').append(`<li class="list-group-item">${valor.nome} 
            <btn class="btn btn-primary editarParticipante" chave="${chave}"> Editar </btn> 
            <btn class="btn btn-secondary excluirParticipante" chave="${chave}"> Excluir</btn> 
            </li> `);
        });
    } else {
        $('#divParticipantesCadastrados').hide();
    }
}